package org.yellow.jasyptutil;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JasyptutilApplication {

	public static void main(String[] args) {
		SpringApplication.run(JasyptutilApplication.class, args);
	}

}
